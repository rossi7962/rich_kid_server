const express = require('express');
const app = express();
const port = 4000;
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;


app.use(function (req, res, next) {
    const origin = req.get('origin');

    // TODO Add origin validation
    res.header('Access-Control-Allow-Origin', origin);
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Cache-Control, Pragma');

    next();
});

app.post('/', (req, res) => {
    const HTTP = new XMLHttpRequest();

    //parameters send to MoMo get get payUrl
    let endpoint = "https://test-payment.momo.vn/gw_payment/transactionProcessor";
    let hostname = "https://test-payment.momo.vn";
    let path = "/gw_payment/transactionProcessor";
    let partnerCode = "MOMO";
    let accessKey = "F8BBA842ECF85";
    let serectkey = "K951B6PE1waDMi640xX08PD3vg6EkVlz";
    let orderInfo = "Prove that you're Rich Kid!";
    let returnUrl = "https://momo.vn/return";
    let notifyurl = "https://callback.url/notify";
    let amount = "20000";
    let orderId = Math.floor(Math.random() * Math.floor(100000000))+ "";
    let requestId = orderId;
    let requestType = "captureMoMoWallet";
    let extraData = "merchantName=;merchantId=";

    let rawSignature = "partnerCode=" + partnerCode + "&accessKey=" + accessKey + "&requestId=" + requestId + "&amount=" + amount + "&orderId=" + orderId + "&orderInfo=" + orderInfo + "&returnUrl=" + returnUrl + "&notifyUrl=" + notifyurl + "&extraData=" + extraData;
    const crypto = require('crypto');
    let signature = crypto.createHmac('sha256', serectkey)
        .update(rawSignature)
        .digest('hex');

    //json object send to MoMo endpoint
    let body = {
        partnerCode: partnerCode,
        accessKey: accessKey,
        requestId: requestId,
        amount: amount,
        orderId: orderId,
        orderInfo: orderInfo,
        returnUrl: returnUrl,
        notifyUrl: notifyurl,
        extraData: extraData,
        requestType: requestType,
        signature: signature,
    };

    HTTP.onreadystatechange = function () {
        if (HTTP.responseText && HTTP.readyState === 4) {
            res.send(JSON.parse(HTTP.responseText));
        }
    }

    HTTP.open("POST", endpoint);
    HTTP.send(JSON.stringify(body));
});

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))